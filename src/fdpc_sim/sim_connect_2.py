import socket
from threading import Thread

class SIM_CONNECT(Thread):
    def __init__(self):

        Thread.__init__(self)
        Thread.deamon = True
        
        self.UDP_IP = "192.168.1.240"
        self.UDP_PORT = 5005

        self.sock = socket.socket(socket.AF_INET,
                             socket.SOCK_DGRAM)
        self.sock.bind((self.UDP_IP, self.UDP_PORT))
##        self.yaw = 0.0
##        self.pitch = 0.0
##        self.roll = 0.0
##        self.sim_ang_to_deg = (360.0/(65536*65536))
##        self.nz = 0.0
##        self.alt = 0.0
##        self.ias = 0.0
        self.AD = list()
        self.INS = list()


    def run(self):
        while True:
            data, addr = self.sock.recvfrom(1024)
            data = bytes(data)
            data = data.decode()
            data = data.replace(' ','')
            data = data.split(',')

##            data = data.replace('[','')
##            data = data.replace(']','')
##            data = data.replace(' ','')
##            data = data.split(',')

            # Read and save correresponding message
            if data[0] == '$AD':
                self.AD = data[1:]

            elif data[0] == '$INS':
                self.INS = data[1:]

            else:
                print('Unknown message identifier: ')
                print(data[0])

            
##            self.ias = float(data[1])/128.0
##            self.yaw = float(data[2])*self.sim_ang_to_deg
##            self.pitch = float(data[3])*self.sim_ang_to_deg*-1
##            self.roll = float(data[4])*self.sim_ang_to_deg*-1
##            self.nz = float(data[7])
##            self.alt = float(data[8])
##    print(data)

    @property
    def yaw(self):
        try:
            return float(self.INS[0])
        except IndexError:
            return 0.0

    @property
    def pitch(self):
        try:
            return float(self.INS[1])*-1.0
        except IndexError:
            return 0.0

    @property
    def roll(self):
        try:
            return float(self.INS[2])*-1.0
        except IndexError:
            return 0.0

    @property
    def nz(self):
        try:
            return float(self.INS[5])
        except IndexError:
            return 0.0

    @property
    def tas(self):
        try:
            return float(self.AD[0])
        except IndexError:
            return 0.0

    @property
    def ias(self):
        try:
            return float(self.AD[1])
        except IndexError:
            return 0.0

    @property
    def alt(self):
        try:
            return float(self.AD[2])
        except IndexError:
            return 0.0

    @property
    def mach(self):
        try:
            return float(self.AD[3])
        except IndexError:
            return 0.0

    @property
    def vsi(self):
        try:
            return float(self.AD[4])
        except IndexError:
            return 0.0

    @property
    def aoa(self):
        try:
            return float(self.AD[5])
        except IndexError:
            return 0.0

    @property
    def aos(self):
        try:
            return float(self.AD[6])
        except IndexError:
            return 0.0
        
    
                     
