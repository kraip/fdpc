### FDPC GPS
import math
import serial
import time
import threading
from fdpc_sensors.serial_reader import ReadLine
from threading import Thread
import RPi.GPIO as GPIO

class GPS(Thread):
    """GPS class, reading and storing data from GPS sensor.
    Supported sentences:
    GPGGA - Time, position, and fix related data.
    GPGSA - GPS DOP and active satellites.
    GPGSV - Number of SVs in view, PRN, elevation, azimuth, and SNR.
    GPRMC - Position, Velocity, and Time
    GPVTG - Actual track made good and speed over ground
    """
    
    def __init__(self,serial_port,receiver_accuracy=2.5):
        """Attributes are:
        serial_port - serial port, where GPS is connected.
        receiver_accuracy - Accuracy of the GPS receiver in meters.
        validity - Indication of GPS validity.
        msg_time_stamp - Time stamp to correct times.
        serCom - Serial read object
        """

        # Setup threading
        Thread.__init__(self)
        Thread.deamon = True

        # Accuracy of GPS receiver (SparkFun Venus GPS = 2.5m)
        self.rec_accy = receiver_accuracy

        # Validity flag, and time since nav lock signal detection
        self.validity = 0.0
        self.nav_lock_ts = 0.0

        # Time stamp when UTC time was read from GPGGA sentence
        self.utc_ts = 0.0

        # NMEA sentences
        self.GPGGA = list()
        self.GPGSA = list()
        self.GPGSV = dict()
        self.GPRMC = list()
        self.GPVTG = list()

        # Serial port
        self.serCom = ReadLine(serial_port)

        # GPIO
        self.nav_lock_gpio = 27


    # GPGGA sentence

    @property
    def UTC(self):
        """Return UTC in HHMMSS.SS format, corrected with message
        time stamp"""
        
        try:
            time_corr = time.time() - self.utc_ts
            return float(self.GPGGA[1])+time_corr
        except IndexError:
            return 000000.00

    @property
    def UTC_h(self):
        """UTC of position fix, hours"""
        
        try:
            return float(self.GPGGA[1][:2])
        except IndexError:
            return 00.0

    @property
    def UTC_m(self):
        """UTC of position fix, minutes"""
        
        try:
            return float(self.GPGGA[1][2:4])
        except IndexError:
            return 00.0

    @property
    def UTC_s(self):
        """UTC of position fix, seconds, corrected with message
        time stamp
        """
        
        try:
            time_corr = time.time() - self.utc_ts
            return float(self.GPGGA[1][4:])+time_corr
        except IndexError:
            return 00.0

    @property
    def lat_dir(self):
        """Direction of latitude N/S"""
        
        try:
            return self.GPGGA[3]
        except IndexError:
            return 'X'

    @property
    def lat(self):
        """Latitude in radians"""
        
        try:
            lat_deg = float(self.GPGGA[2][:2])
            lat_min = float(self.GPGGA[2][2:])
            lat_rad = math.radians(lat_deg + lat_min/60.0)
            if self.lat_dir=='S':
                lat_rad = -lat_rad
            return lat_rad
        except IndexError:
            return 0.0

    @property
    def lat_deg(self):
        """Degrees of latitude.
        See lat_min and lat_sec for minutes and seconds.
        """
        
        try:
            return float(self.GPGGA[2][:2])
        except IndexError:
            return 0.0

    @property
    def lat_min(self):
        """Minutes of latitude.
        See lat_deg and lat_sec for degrees and seconds.
        """

        try:
            return float(self.GPGGA[2][2:4])
        except IndexError:
            return 0.0

    @property
    def lat_sec(self):
        """Seconds of latitude.
        See lat_deg and lat_min for degrees and minutes.
        """

        try:
            return float(self.GPGGA[2][4:])*60.0
        except IndexError:
            return 0.0

    @property
    def lat_dms(self):
        """Latitude in degrees-minutes-seconds format.
        Uses the lat_deg, lat_min, lat_sec decorators
        Returned as a 16 char string.
        """

        try:
            deg_sign = u'\xb0'
            dms = (self.lat_dir + '  '
                   +'{:02.0f}'.format(self.lat_deg) + deg_sign
                   + '{:02.0f}'.format(self.lat_min) + '\''
                   +'{:06.3f}\" '.format(self.lat_sec))
            return dms                
        except IndexError:
            return '00'+ u'\xb0' + '00\'' + '00.000" X'

    @property
    def lon_dir(self):
        """Direction of longitude, E/W"""
        
        try:
            return self.GPGGA[5]
        except IndexError:
            return 'X'

    @property
    def lon(self):
        """Longitude in radians"""
        
        try:
            lon_deg = float(self.GPGGA[4][:3])
            lon_min = float(self.GPGGA[4][3:])
            lon_rad = math.radians(lon_deg + lon_min/60.0)
            if self.lon_dir=='W':
                lon_rad = -lon_rad
            return lon_rad
        except IndexError:
            return 0.0

    @property
    def lon_deg(self):
        """Degrees of lonitude.
        See lon_min and lon_sec for minutes and seconds.
        """
        
        try:
            return float(self.GPGGA[4][:3])
        except IndexError:
            return 0.0

    @property
    def lon_min(self):
        """Minutes of longitude.
        See lon_deg and lon_sec for degrees and seconds.
        """

        try:
            return float(self.GPGGA[4][3:5])
        except IndexError:
            return 0.0

    @property
    def lon_sec(self):
        """Seconds of longitude.
        See lon_deg and lon_min for degrees and minutes.
        """

        try:
            return float(self.GPGGA[4][5:])*60.0
        except IndexError:
            return 0.0

    @property
    def lon_dms(self):
        """Longitude in degrees-minutes-seconds format.
        Uses the lon_deg, lon_min, lon_sec decorators
        Returned as a 16 char string
        """

        try:
            deg_sign = u'\xb0'
            dms = (self.lon_dir + ' '
                   + '{:03.0f}'.format(self.lon_deg) + deg_sign
                   + '{:02.0f}'.format(self.lon_min) + '\''
                   +'{:06.3f}\" '.format(self.lon_sec))
            return dms                
        except IndexError:
            return  '000'+ u'\xb0' + '00\'' + '00.000" X'

    @property
    def quality(self):
        """GPS Quality indicator
        0: Fix not valid
        1: GPS fix
        2: Differential GPS fix, OmniSTAR VBS
        4: Real-Time Kinematic, fixed integers
        5: Real-Time Kinematic, float integers,
        OmniSTAR XP/HP or Location RTK
        """
        
        try:
            return int(self.GPGGA[6])
        except IndexError:
            return 0

    @property
    def n_satellites(self):
        """Number of SVs in use, range from 00 through to 24+"""
        try:
            return int(self.GPGGA[7])
        except IndexError:
            return 0

    @property
    def HDOP_GGA(self):
        """Horizontal dulution of position.
        <1: Ideal
        1-2: Excellent
        2-5: Good
        5-10: Moderate
        10-20: Fair
        >20: Poor
        20 is returened in case of error

        Note: The HDOP value is also available from self.HDOP,
        which reads the value from the GSA sentence.
        """
        
        try:
            return float(self.GPGGA[8])
        except IndexError:
            return 20.0

    @property
    def h_geo(self):
        """Height of the antenna (Height above geoide) normaly
        in meters [m]
        """

        try:
            return float(self.GPGGA[9])
        except IndexError:
            return 0.0

    @property
    def h_geo_unit(self):
        """Unit of height M = Meters, F = Feet"""

        try:
            return self.GPGGA[10]
        except IndexError:
            return 'X'

    @property
    def h_ell(self):
        """Height Above Ellipsoid (HAE). Using geoidal separation
        (GPGGA[11]).
        Subtract this from the altitude of the
        antenna to arrive at the Height Above Ellipsoid (HAE).
        """

        try:
            geo_sep = float(self.GPGGA[11])
            return self.h_geo - geo_sep
        except IndexError:
            return 0.0

    @property
    def h_ell_unit(self):
        """Unit of height M = Meters, F = Feet"""

        try:
            return self.GPGGA[12]
        except IndexError:
            return 'X'

    @property
    def DGPS_record_age(self):
        """Age of differential GPS data record, Type 1 or Type 9.
        Null field when DGPS is not used.
        """

        try:
            return self.GPGGA[13]
        except IndexError:
            return None

    @property
    def ref_station_ID(self):
        """Reference station ID, range 0000-4095. A null field
        when any reference station ID is selected and no
        corrections are received
        """

        try:
            return int(self.GPGGA[14][:4])
        except IndexError:
            return None

    @property
    def GGA_checksum(self):
        """The checksum data of the GGA sentence,
        always begins with *
        """

        try:
            checksum = self.GPGGA[14][4:]
        except IndexError as e:
            return None
        else:
            return checksum.replace('\r\n','')



    # GPGSA sentence

    @property
    def mode_1(self):
        """Mode 1, M = manual, A = automatic"""

        try:
            return self.GPGSA[1]
        except IndexError:
            return 'X'

    @property
    def mode_2(self):
        """Mode 2, Fix type, 1 = not available, 2 = 2D, 3 = 3D"""

        try:
            return int(self.GPGSA[2])
        except IndexError:
            return 1

    @property
    def active_PRNs(self):
        """PRN number, 01 through 32 for GPS, 33 through 64 for SBAS,
        64+ for GLONASS
        """

        try:
            return self.GPGSA[3:15]
        except IndexError:
            pass

    @property
    def PDOP(self):
        """Position dulution of position.
        <1: Ideal
        1-2: Excellent
        2-5: Good
        5-10: Moderate
        10-20: Fair
        >20: Poor
        99.9 is returened in case of error
        """

        try:
            return float(self.GPGSA[15])
        except IndexError:
            return 99.9

    @property
    def HDOP(self):
        """Position dulution of position.
        <1: Ideal
        1-2: Excellent
        2-5: Good
        5-10: Moderate
        10-20: Fair
        >20: Poor
        99.9 is returened in case of error
        """

        try:
            return float(self.GPGSA[16])
        except IndexError:
            return 99.9

    @property
    def VDOP(self):
        """Vertical dulution of position.
        <1: Ideal
        1-2: Excellent
        2-5: Good
        5-10: Moderate
        10-20: Fair
        >20: Poor
        99.9 is returened in case of error
        """

        try:
            return float(self.GPGSA[17][:3])
        except IndexError:
            return 99.9

    @property
    def GSA_checksum(self):
        """The checksum of GPGSA sentence, always begins with *"""

        try:
            checksum = self.GPGSA[17][3:]
        except IndexError as e:
            return None
        else:
            return checksum.replace('\r\n','')


    # GPGSV sentence

    @property
    def n_GSV_msgs(self):
        """Total number of GSV-messages of this type in this cycle."""

        try:
            avail_keys = list(self.GPGSV.keys())
            gsv_msg = self.GPGSV[avail_keys[0]]
            n_sats = int(gsv_msg[1])
        except (KeyError, IndexError):
            return 0
        else:
            return n_sats

    @property
    def satellite_info(self):
        """Dictionary containing information about all sattelites
        in view
        """
        
        sats_info = dict()
        for keys, msg in self.GPGSV.items():

            n = 4
            while n <= 16:
                try:
                    pnr = int(msg[n])
                except (ValueError, IndexError):
                    break
                else:
                    elev = msg[n+1]
                    azim = msg[n+2]
                    snr = msg[n+3][:2]
                    sats_info[pnr] = [elev, azim, snr]
                    n+=4
        return sats_info

    @property
    def GSV_checksum(self):
        """Checksum of the GPGSV sentence"""

        try:
            avail_keys = list(self.GPGSV.keys())
            gsv_msg = self.GPGSV[avail_keys[0]]
            
        except (KeyError, IndexError):
            return 0

        else:
            checksum = gsv_msg[19][2:]
            return checksum.replace('\r\n','')



    # GPRMC sentence

    @property
    def data_status(self):
        """GPS data status
        A=data valid or V=data not valid
        A valid status is derived from the SiRF Binary M.I.D 2
        position mode 1. See the SiRF Binary Protocol Reference Manual.
        """

        try:
            return self.GPRMC[2]
        except IndexError:
            return 'V'

    @property
    def GS(self):
        """Speed over ground. Converted to m/s."""

        try:
            return float(self.GPRMC[7])*0.5144447
        except IndexError:
            return 0.0

    @property
    def TA(self):
        """Track angle in degrees (TRUE)"""

        try:
            return float(self.GPRMC[8])
        except IndexError:
            return 0.0

    @property
    def date(self):
        """Date in format ddmmyy"""

        try:
            return self.GPRMC[9]
        except IndexError:
            return 'DDMMYY'

    @property
    def RMC_checksum(self):
        """The checksum of GPRMC sentence, always begins with *"""

        try:
            checksum = self.GPRMC[12][1:]
        except IndexError as e:
            return None
        else:
            return checksum.replace('\r\n','')
        
    

    def check_valid(self):
        """Validity checking. This method runs checks on selected
        enteties to determine if data from GPS is valid.
        Firtly it checks NMEA sentences. Lastly it checks time since
        detection of nav lock signal from gps, read via GPIO.
        """

        # Constants
        NOT_VALID = 0.0
        VALID = 1.0
        FIX_3D = 3

        # Status in NMEA sentence.
        if self.data_status == 'V':
            self.validity = NOT_VALID

        # Fix is not three dimentional.
        elif self.mode_2 < FIX_3D:
            self.validity = NOT_VALID

        # All checks passed.    
        else:
            self.validity = VALID

        # Hardware signal of validity. Check if signal was detected,
        # and calculate time since last nav lock signal detection.
        # If longer than 3 seconds since last detection, set
        # validity to not valid.
        if GPIO.event_detected(self.nav_lock_gpio):
            self.nav_lock_ts = time.time()
        time_since_lock = time.time() - self.nav_lock_ts
        if time_since_lock > 3.0:
            self.validity = NOT_VALID
        
        
    def save_GPGSV(self,msg_str):
        """Method for saving the GPGSV message and sorting satellite
        info. self.GPGSV is a dictionary.
        """

        # Save the most recent message in the GPGSV dict.
        msg_nr = int(msg_str[2])
        self.GPGSV[msg_nr] = msg_str

        # Check if there are any unavailable messages, i.e. messages
        # that have been stored, but are no longer available from gps.
        # If so, remove unavalable messages from GPGSV dictionary.
        n_msgs = int(msg_str[1])
        n_stored_keys = list(self.GPGSV.keys())
        unavail_msgs = n_stored_keys[-1] - n_msgs
        if unavail_msgs > 0:
            del self.GPGSV[-unavail_msgs:]        
            
    
    def updateData(self):
        """Update function of GPS class."""
        
        try:            
            msg_str = bytes(self.serCom.readline())
            msg_str = msg_str.decode()
            msg_str = msg_str.split(',')
            
        except Exception:
            self.validity = 0.0

        else:          
            # Read and save corresponding message
            if msg_str[0] == '$GPGGA':
                self.utc_ts = time.time()
                self.GPGGA = msg_str

            elif msg_str[0] == '$GPGSA':
                self.GPGSA = msg_str

            elif msg_str[0] == '$GPGSV':
                self.save_GPGSV(msg_str)

            elif msg_str[0] == '$GPRMC':
                self.GPRMC = msg_str

            elif msg_str[0] == '$GPVTG':
                self.GPVTG = msg_str

            else:
                print('GPS --- in updateData() --- ',
                      'Unknown or error message:')
                print(msg_str)

    def run(self):
        """Run method.
        The method that executes when gps.start() is called
        """

        # Setup GPIO for NAV-lock signal (this is considered in
        # check_valid)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.nav_lock_gpio,
                   GPIO.IN,
                   pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.nav_lock_gpio,
                              GPIO.RISING)

        # Loop running data update and validity checking
        while True:
            self.updateData()
            self.check_valid()




            
### Testing
##gpsSerial = serial.Serial(port='/dev/serial0', baudrate = 9600, timeout = 0.02)
##gps = GPS(gpsSerial)
##gps.start()
##
##def main():
##    while True:
##        print('Validity: {}'.format(gps.validity))
##        print(' ')
##
##        # GGA
##        print(gps.GPGGA)
##        print('Lat: ' + gps.lat_dms)
##        print('Lat[rad]: {}'.format(gps.lat))
##        print('Lon: ' + gps.lon_dms)
##        print('Lon[rad]: {}'.format(gps.lon))
##        print('UTC: {}'.format(gps.UTC))
##        print('UTC_s: {}'.format(gps.UTC_s))
##        print('Quality: {}'.format(gps.quality))
##        print('No of sats: {}'.format(gps.n_satellites))
##        print('HDOP: {}'.format(gps.HDOP))
##        print('h_geoid: {}m'.format(gps.h_geo))
##        print('h_geo_unit: {}'.format(gps.h_geo_unit))
##        print('h_ell: {}m'.format(gps.h_ell))
##        print('h_ell_unit: {}'.format(gps.h_ell_unit))
##        print('DGPS rec age: {}'.format(gps.DGPS_record_age))
##        print('Ref station ID: {}'.format(gps.ref_station_ID))
##        print('GGA C/S: {}'.format(gps.GGA_checksum))
##        print('-------------- ')
##        print(' ')
##
##
##        # GSA
##        print(gps.GPGSA)
##        print('Mode 1: ' + gps.mode_1)
##        print('Mode 2: {}'.format(gps.mode_2))
##        print('Active PRNs: ' + str(gps.active_PRNs))
##        print('PDOP: {}'.format(gps.PDOP))
##        print('HDOP: {}'.format(gps.HDOP))
##        print('VDOP: {}'.format(gps.VDOP))
##        print('Pos accy: {}m'.format(gps.pos_accy))
##        print('Horz accy: {}m'.format(gps.horz_accy))
##        print('Vert accy: {}m'.format(gps.vert_accy))
##        print('GSA C/S: {}'.format(gps.GSA_checksum))
##        print('-------------- ')
##        print(' ')
##
##
##        # GSV
##        for key, msg in gps.GPGSV.items():
##            print(msg)
##        print('nr of GSV messages: {}'.format(gps.n_GSV_msgs))
##        
##        for key, sat_info in gps.satellite_info.items():
##            print('PNR {} info: {}'.format(key,sat_info))
##        print('GSV C/S: {}'.format(gps.GSV_checksum))
##        print('-------------- ')
##        print(' ')
##
##
##        # RMC
##        print(gps.GPRMC)
##        print('Data status: {}'.format(gps.data_status))
##        print('Ground speed [m/s]: {}'.format(gps.GS))
##        print('True Track [deg]: {}'.format(gps.TA))
##        print('Date: {}'.format(gps.date))
##        print('RMC C/S: {}'.format(gps.RMC_checksum))
##        print('-------------- ')
##        print(' ')
##        
##        print(gps.GPVTG)
##        print('-------------- ')
##        print(' ')
##              
##        time.sleep(1)
##        
##
##
##
##if __name__ == '__main__':main()



##b'$GPGGA,201159.741,5823.5693,N,01537.2664,E,1,06,2.2,45.1,M,31.0,M,,0000*67\r\n'

##b'$GPGSA,A,3,08,21,11,27,10,18,,,,,,,2.9,2.2,1.8*36\r\n'

##b'$GPGSV,3,1,12,08,66,262,50,10,64,095,34,27,54,166,37,18,38,266,32*7B\r\n'
##b'$GPGSV,3,2,12,11,26,281,51,32,20,135,32,28,18,330,19,20,12,054,27*7B\r\n'
##b'$GPGSV,3,3,12,21,10,093,38,15,09,024,31,22,05,219,36,30,02,303,*7B\r\n'

##b'$GPRMC,201159.741,A,5823.5693,N,01537.2664,E,000.0,000.0,290418,,,A*67\r\n'

##b'$GPVTG,000.0,T,,M,000.0,N,000.0,K,A*0D\r\n'
##b''
##b''
##b''
