"""FDPC AHRS module code.
Created by: Christian Krantz (mailto:krippekrantz@hotmail.com)

Requriements:
    - serial
    - numpy

Python 3.5 (Tested with v3.5.3)
"""

import serial
import numpy as np
from fdpc_sensors.serial_reader import ReadLine
from threading import Thread

class AHRS(Thread):

    # Input arguments are: serialPort and mode.
    # Both input arguments are optional. If no args. are supplied,
    # standard settings will be used.
    def __init__(self, serialPort):

        Thread.__init__(self)
        Thread.deamon = True

        # Setup serial communication
        self.serialPort = serialPort
        self.serCom = ReadLine(serialPort)

        # Data validity
        self.validity = 0.0

        # Attitude angles [rad]
        self.yaw = 0.0
        self.pitch = 0.0
        self.roll = 0.0

        # Accelerations [g]
        self.nx = 0.0
        self.ny = 0.0
        self.nz = 0.0

        # Angular rates [rad/s]
        self.roll_rate = 0.0
        self.pitch_rate = 0.0
        self.yaw_rate = 0.0
        
        # Set output mode of YPRAG, by sending b'#ox' to the sensor.
        # Yaw, Pitch, Roll, Accelerometers, Gyros (see Arduino sketch).
        serialPort.write(b'#ox')
        

    @property
    def data_vec(self):
        """"Ability to return AHRS data as an array"""
        return np.array([self.validity,
                        self.yaw,
                        self.pitch,
                        self.roll,
                        self.nx,
                        self.ny,
                        self.nz,
                        self.roll_rate,
                        self.pitch_rate,
                        self.yaw_rate])


    def update_data(self):
        """Read serial data and update AHRS parameters"""

        # Read data from serialPort
        try:
            data_str = bytes(self.serCom.readline())
            data_str = data_str.decode()

        # Set validity to 0 (not valid) if failed to read data
        except Exception:
            self.validity = 0.0

        # Read, interpret and store serial data
        else:

            # Message in the correct format
            if len(data_str)>0 and data_str[0]=='#':            
                data_str = data_str.replace('#YPRAG=','')
                data_str = data_str.split(',')
                self.validity = 1.0
                self.yaw = float(data_str[0])*0.01745
                self.pitch = float(data_str[1])*0.01745
                self.roll = float(data_str[2])*0.01745
                self.nx = float(data_str[3])
                self.ny = float(data_str[4])
                self.nz = float(data_str[5])
                self.roll_rate = float(data_str[6])
                self.pitch_rate = float(data_str[7])
                self.yaw_rate = float(data_str[8])

            # Set validity to 0 (not valid) and print to terminal
            else:
                self.validity = 0.0
                print('AHRS --- in updateData() --- Unknown or error message:')
                print(data_str)

    def run(self):
        while True:
            self.update_data()


# Main function for debuging and testing
##AHRS_UART = serial.Serial(port='/dev/ttyUSB0', baudrate = 57600)
##ahrs = AHRS(AHRS_UART)
##ahrs.start()
##def main():
##    while True:
##        print(ahrs.data_vec)
##        pass
##        
##if __name__ == '__main__':main()
            
            






        
