import pyuipc
import socket


# Setup UDP
UDP_IP = "192.168.1.240"
UDP_PORT = 5005
sock = socket.socket(socket.AF_INET,
                     socket.SOCK_DGRAM)

# Setup pyuipc
pyuipc.open(pyuipc.SIM_FSX)

#---------------------------------------------------------------------
#--- AIR DATA - Definitions from FSUIPC offsets
tas = (0x02B8, 'd')         # True Air Speed
ias = (0x02BC, 'd')         # Indicated Air Speed
speed_conv_fact = 1/128.0   # Convertion factor for speeds

p_alt = (0x34B0,'f');       # Pressure altitude

mach = (0x11C6, 'h')        # Mach number
mach_conv = 1/20480.0           # Mach number convertion factor

vert_speed = (0x0842, 'h')  # Vertical speed [m/min]
vert_speed_conv = -3.28084  # Convertion factor for v-speed

aoa = (0x2ED0, 'f')         # Angle of attack
aos = (0x2ED8, 'f')         # Angle of side slip

# List to read from FSUIPC
AD_list_raw = [tas, ias, p_alt, mach, vert_speed, aoa, aos]
AD_to_read = pyuipc.prepare_data(AD_list_raw)


#---------------------------------------------------------------------
#--- INERTIAL DATA - Definitions from FSUIPC offsets
heading = (0x0580,'d')      # Heading [degree*65536*65536]
pitch = (0x0578,'d')        # Pitch [degree*65536*65536]
roll = (0x057C,'d')         # Roll [degree*65536*65536]
ang_to_deg_fact = 360.0/(65536*65536) # Convertion factor to degrees

##pitch_rate = (0x30A8,'')
##roll_rate = (0x30B0,'')
nx = (0x3070, 'f')          # Longitudinal acceleration [ft/s^2]
ny = (0x3060, 'f')          # Lateral acceleration [ft/s^2]
nz = (0x1140, 'f')          # Vertical acceleration [g]

# List to read from FSUIPC
INS_list_raw = [heading, pitch, roll, nx, ny, nz]
INS_to_read = pyuipc.prepare_data(INS_list_raw)


#---------------------------------------------------------------------
#--- GPS DATA - Definitions from FSUIPC offsets
##    GPS_data = pyuipc.read()
##    GPS_data[0] = lat
##    GPS_data[0] = lon
##    GPS_data[0] = UTC
##    GPS_data[0] = quality
##    GPS_data[0] = n_sats
##    GPS_data[0] = h_geo
##    GPS_data[0] = h_el
##    GPS_data[0] = mode_1
##    GPS_data[0] = mode_2
##    GPS_data[0] = GS
##    GPS_data[0] = track_angle
##    GPS_data[0] = date
##    GPS_data[0] = HDOP
##    GPS_data[0] = PDOP
##    GPS_data[0] = VDOP



#---------------------------------------------------------------------
#--- Read and convert data from FSUIPC
##data_list = [TAS, IAS, heading, pitch, roll, nx, ny, nz, p_alt]
##dat = pyuipc.prepare_data(data_list)


while True:
    # Air data
    AD_data = pyuipc.read(AD_to_read)       # Read
    AD_data[0] = AD_data[0]*speed_conv_fact # Convert TAS
    AD_data[1] = AD_data[1]*speed_conv_fact # Convert IAS
    AD_data[3] = AD_data[3]*mach_conv       # Convert Mach
    AD_data[4] = AD_data[4]*vert_speed_conv # Convert vertical speed
    AD_data_str = str(AD_data)              # Convert data to string
    AD_data_str = AD_data_str[1:-1]         # Remove brackets from string
    AD_MSG = '$AD,' + AD_data_str           # Add tag to data

    # Inertial data
    INS_data = pyuipc.read(INS_to_read)
    INS_data[0] = INS_data[0]*ang_to_deg_fact
    INS_data[1] = INS_data[1]*ang_to_deg_fact
    INS_data[2] = INS_data[2]*ang_to_deg_fact
    INS_data_str = str(INS_data)            # Convert data to string
    INS_data_str = INS_data_str[1:-1]       # Remove brackets from string
    INS_MSG = '$INS,' + INS_data_str        # Add tag to data

    # GPS data
##    GPS_data = pyuipc.read()
##    GPS_data[0] = lat
##    GPS_data[0] = lon
##    GPS_data[0] = UTC
##    GPS_data[0] = quality
##    GPS_data[0] = n_sats
##    GPS_data[0] = h_geo
##    GPS_data[0] = h_el
##    GPS_data[0] = mode_1
##    GPS_data[0] = mode_2
##    GPS_data[0] = GS
##    GPS_data[0] = track_angle
##    GPS_data[0] = date
##    GPS_data[0] = HDOP
##    GPS_data[0] = PDOP
##    GPS_data[0] = VDOP
##    GPS_MSG = '$GPS,' + GPS_data_str        # Add tag to data




    # Send data to hand held unit
    sock.sendto(AD_MSG, (UDP_IP, UDP_PORT))
    sock.sendto(INS_MSG, (UDP_IP, UDP_PORT))
##    sock.sendto(GPS_MSG, (UDP_IP, UDP_PORT))
    
##    MSG = str(out)
##    sock.sendto(MSG, (UDP_IP, UDP_PORT))
##    print(out[7])
##    print(AD_MSG)
pyuipc.close()








