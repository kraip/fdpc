#!/usr/bin/python
"""
Main file for the Flight Data and Planning Computer
Created by: Christian Krantz
"""

# Flag to set simulated or real sensors
SIM = True
##BIG_SCREEN = False
BIG_SCREEN = True


### -------------------------------------------------------------------|
# --- Import
# Libraries
import math
import pi3d
import serial
import serial.tools.list_ports
import threading
import time
import numpy as np
import RPi.GPIO as GPIO

# FDPC packages
from fdpc_graphical.navball import NavBall
from fdpc_graphical.altitude_dail import Dail
from fdpc_graphical.digital_readout import digitalReadout
from fdpc_graphical.readout_bar import readoutBar
from fdpc_sensors.ahrs import AHRS
from fdpc_sensors.gps import GPS
from fdpc_sensors.serial_reader import ReadLine
from fdpc_filter.low_pass import moving_average
from fdpc_filter.low_pass import exp_smooth

# UDP
import socket
from fdpc_sim.sim_connect_2 import SIM_CONNECT

# Backlight settings
##import rpi_backlight as bl
##bl.set_brightness(255)


### -------------------------------------------------------------------|
# --- Constants
DEG2RAD = math.pi/180.0


### -------------------------------------------------------------------|
# --- Colors and fonts
COLOR_DAY_BACKGROUND = np.array([255/255,255/255,204/255])
COLOR_DARK_LINES = np.array([0.0, 0.0, 0.0])/255
STD_FONT = '../fonts/Courier Prime Sans.ttf'
font = pi3d.Font(STD_FONT, color=(0,0,0,255), font_size=20)



### -------------------------------------------------------------------|
# --- Display-, camera- and shader settings
DISP_WIDTH = 480        # Width
DISP_HEIGHT = 800       # Height

if BIG_SCREEN:
    DISP_POS_X = 1920-DISP_WIDTH    # Position X
    DISP_POS_Y = 0                  # Position Y
else:
    DISP_POS_X = 0          # Position X
    DISP_POS_Y = 0          # Position Y

DISP_FPS = 60           # Framerate
DISP_ANTIALIASING = 4   # Anti-alaising ON, (OFF = 0)

DISPLAY = pi3d.Display.create(x = DISP_POS_X, y = DISP_POS_Y,
                              frames_per_second = DISP_FPS,
                              samples = DISP_ANTIALIASING)
DISPLAY.set_background(COLOR_DAY_BACKGROUND[0],
                       COLOR_DAY_BACKGROUND[1],
                       COLOR_DAY_BACKGROUND[2],1)
DISPLAY.resize(x=DISP_POS_X,y=DISP_POS_Y ,w=DISP_WIDTH,h=DISP_HEIGHT)

DISP_SHADER_FLAT = pi3d.Shader("uv_flat")   # 3D Shader flat
DISP_CAM_POS = (0.0,0.6,-6.0)             # Camera- & ADI position
DISP_CAM_DIRECTION = (0.0,0.0,0.0)          # Where camera is pointing
DISP_CAMERA_3D = pi3d.Camera(at = DISP_CAM_DIRECTION,   # Create camera
                             eye = DISP_CAM_POS)


### -------------------------------------------------------------------|
# --- Create screen ref lines
# Vertical line center
midLineX = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(0.0,DISP_HEIGHT/2,0.0),
                                        (0.0,-DISP_HEIGHT/2,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineX.set_shader(pi3d.Shader("mat_flat"))

# Horizontal line center
midLineYC = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(-DISP_WIDTH/2,0.0,0.0),
                                        (DISP_WIDTH/2,0.0,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineYC.set_shader(pi3d.Shader("mat_flat"))

# Horizontal line 1/4rd
midLineY14 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(-DISP_WIDTH/2,DISP_HEIGHT/4,0.0),
                                        (DISP_WIDTH/2,DISP_HEIGHT/4,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineY14.set_shader(pi3d.Shader("mat_flat"))

# Horizontal line 3/4th
midLineY34 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(-DISP_WIDTH/2,-DISP_HEIGHT/4,0.0),
                                        (DISP_WIDTH/2,-DISP_HEIGHT/4,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineY34.set_shader(pi3d.Shader("mat_flat"))



### -------------------------------------------------------------------|
# --- Create ADI/NavBall
ADI_SELFLIGHT = pi3d.Light(lightamb=(1.1, 1.1, 1.1))
ADI_TEXTURE = pi3d.Texture("../textures/navball_new.png")
ADI_RADI = 1.0
ADI_POS = (DISP_CAM_POS[0],DISP_CAM_POS[1],0)
ADI_CROSSHAIR_SCALE = 0.6
adi = NavBall(ADI_TEXTURE, DISP_SHADER_FLAT, ADI_RADI,
              light = ADI_SELFLIGHT,
              pos = ADI_POS,
              crossHairScaleFactor = ADI_CROSSHAIR_SCALE)
vel_vec_aoa_exp_filter = exp_smooth(factor=0.25)
vel_vec_aos_exp_filter = exp_smooth(factor=0.25)


### -------------------------------------------------------------------|
# --- Create analoge dails
altDail = Dail([150.0,310,0],0.0,10.0,1.0,dailRadi=80,nVerts=48)
speedDail = Dail([-150.0,310,0],0.0,10.0,1.0,dailRadi=80,nVerts=48)
alt_ro = digitalReadout([150.0,310.0,0.0],"{:5.0f}",STD_FONT,[99999])
ias_ro = digitalReadout([-150.0,310.0,0.0],"{:4.0f}",STD_FONT,[9999])
mach_ro = digitalReadout([-150.0,310.0,0.0],"{:4.3f}",STD_FONT,[9.999])

### -------------------------------------------------------------------|
# --- Create digital readouts
yawReadout = digitalReadout([0.0,330.0,0.0],"HDG: {:03.0f}",STD_FONT,[360.0])
pitchReadout = digitalReadout([0.0,310.0,0.0],"PCH: {:3.0f}",STD_FONT,[90.0])
rollReadout = digitalReadout([0.0,290.0,0.0],"ROL: {:4.0f}",STD_FONT,[-180.0])
nzReadout = digitalReadout([-170.0,-50.0,0.0],"G:{:3.1f}",STD_FONT,[90.0])
aoaReadout = digitalReadout([-190.0,220.0,0.0],"AoA:{:2.1f}",STD_FONT,[90.0])
aosReadout = digitalReadout([0.0,-100.0,0.0],"AoS:{:5.1f}",STD_FONT,[90.0])



### -------------------------------------------------------------------|
# --- Create readout bar
# Load factor
nzPosLim = 9.0
nzNegLim = 0.0
nzBar = readoutBar([-190.0,-40.0,0.0],[-190.0,180.0,0.0],
                    nzNegLim,nzPosLim, lineWidth=6,
                   scaleMarkerFlip=True, limit_within_scale=False)
nz_filter = moving_average(nSamples=10)
nz_exp_filter = exp_smooth(factor=0.5)

# AoS
sideSlipPosLim = 3.0
sideSlipNegLim = -3.0
sideSlipBar = readoutBar([-150.0,-80.0,0.0],[150.0,-80.0,0.0],
                         sideSlipNegLim,sideSlipPosLim, lineWidth=4,
                         scaleMarkerFlip=False, limit_within_scale=True)


# AoA
aoaPosLim = 3.0
aoaNegLim = 0.0
aoaBar = readoutBar([-210.0,-10.0,0.0],[-210.0,210.0,0.0],
                         aoaNegLim,aoaPosLim, lineWidth=8,
                         scaleMarkerFlip=False, limit_within_scale=True)

### -------------------------------------------------------------------|
# --- Serial test
##ports = list(serial.tools.list_ports.comports())
##print('Available com-port units:')
##for p in ports:
##    print(p)

### -------------------------------------------------------------------|
# --- Open sim connection
##UDP_IP = "192.168.1.242"
##UDP_PORT = 5005
##
##sock = socket.socket(socket.AF_INET,
##                     socket.SOCK_DGRAM)
##sock.bind((UDP_IP, UDP_PORT))
sim_con = SIM_CONNECT()
sim_con.start()



### -------------------------------------------------------------------|
# --- Set up serial communication and initialize AHRS over UART
##AHRS_UART = serial.Serial(port='/dev/ttyUSB0', baudrate = 57600, timeout=0.02)
##ahrs = AHRS(AHRS_UART)
##ahrs.start()


### -------------------------------------------------------------------|
# --- GPS setup
##gpsSerial = serial.Serial(port='/dev/serial0', baudrate = 9600, timeout = 0.02)
##gps = GPS(gpsSerial)
##gps.start()


### -------------------------------------------------------------------|
# --- Main function
sim_ang_to_deg = (360.0/(65536*65536))/0.01745
def main():

        
    # Start loop
    while DISPLAY.loop_running():
##        if GPIO.event_detected(17):
##            print('Tick')
##
##        if GPIO.event_detected(27):
##            print('GPS OK')

        # Draw screen ref lines
##        midLineX.set_line_width(2.0)
##        midLineX.draw()
##        midLineYC.set_line_width(2.0)
##        midLineYC.draw()
##        midLineY14.set_line_width(1.0)
##        midLineY14.draw()
##        midLineY34.set_line_width(1.0)
##        midLineY34.draw()
                        
        # Draw and rotate NavBall
##        ahrsData = ahrs.data_vec
        vel_vec_aoa_exp_filter.update(sim_con.aoa)
        vel_vec_aos_exp_filter.update(sim_con.aos)
        adi.rotateAndDraw(1,
                          sim_con.yaw,
                          sim_con.pitch,
                          sim_con.roll,
                          aoa = math.degrees(vel_vec_aoa_exp_filter.filtered_data),
                          aos = math.degrees(vel_vec_aos_exp_filter.filtered_data))
##        adi.rotateAndDraw(1,
##                          90.0, 10.0, 45.0)
##        print(float(ahrsData[2])*sim_ang_to_deg)

        # Draw analoge dails
##        adiDail.updateAndDraw()
        alt_to_angle = ((sim_con.alt*3.2808/1000.0)*360)%360
        altDail.updateAndDraw(alt_to_angle)
        alt_ro.updateAndDraw(sim_con.alt*3.2808)

        spd_to_angle = ((sim_con.ias/1000.0)*360)%360
        speedDail.updateAndDraw(spd_to_angle)
        if sim_con.mach >= 0.6:
            mach_ro.updateAndDraw(sim_con.mach)
        else:
            ias_ro.updateAndDraw(sim_con.ias)

##        # Draw digital readout
        yawReadout.updateAndDraw((sim_con.yaw+360.0)%360)
        pitchReadout.updateAndDraw(sim_con.pitch)
        rollReadout.updateAndDraw(sim_con.roll)
        nzReadout.updateAndDraw(sim_con.nz)
        aoaReadout.updateAndDraw((math.degrees(sim_con.aoa)))
        aosReadout.updateAndDraw((math.degrees(sim_con.aos)))

        # Draw and update nz Bar
        nz_exp_filter.update(sim_con.nz)
        nzBar.updateAndDraw(1.0, nz_exp_filter.filtered_data)

        # Draw and update aos bar
        sideSlipBar.updateAndDraw(1.0,(math.degrees(sim_con.aos)/5.0))

        # Draw and update aoa bar
        aoaBar.updateAndDraw(1.0,(math.degrees(sim_con.aoa)/10.0))
        

##        # Draw GPS data
##        utchReadout.updateAndDraw(gps.UTC_h)
##        utcmReadout.updateAndDraw(gps.UTC_m)
##        utcsReadout.updateAndDraw(gps.UTC_s)
##        gps_latDMS_ro.updateAndDraw(gps.lat_dms)
##        gps_lonDMS_ro.updateAndDraw(gps.lon_dms)
##        gps_nSat_ro.updateAndDraw(gps.n_satellites)
##        gps_fix_ro.updateAndDraw(gps.mode_2)
##        gps_hEll_ro.updateAndDraw(gps.h_ell)
##        gps_hGeo_ro.updateAndDraw(gps.h_geo)
##        gps_GS_ro.updateAndDraw(gps.GS*3.6)
##        gps_TT_ro.updateAndDraw(gps.TA)
##        gps_valid_ro.updateAndDraw(gps.validity)

        
        
    # Close Serial port    
##    AHRS_UART.close()
##    gpsSerial.close()
    DISPLAY.destroy()
    GPIO.cleanup()



### -------------------------------------------------------------------|
# --- Exexcution
if __name__ == '__main__':main()






