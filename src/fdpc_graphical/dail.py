import pi3d
import numpy as np
import math

### ----------------------------------------------------------------------------
# --- Analoge dail
class Dail(object):
    def __init__(self, pos, scaleStart, scaleEnd, scaleStep,
                 dailRadi=50,lineWidth=2.0, nVerts = 24):
        self.pos = pos
        self.scaleStart = scaleStart
        self.scaleEnd = scaleEnd
        self.scaleStep = scaleStep
        self.dailRadi = dailRadi
        self.lineWidth = lineWidth
        self.nVerts = nVerts
        
        # Define Dail circle
        circle = self.createCircle()
        self.dail = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                                    vertices = circle,
                                    line_width = self.lineWidth/2.0,
                                    material=(0/255,0/255,0/255))
        self.dail.set_shader(pi3d.Shader("mat_flat"))

        # Define Dail pointer 1
        pointEndX = pos[0]+math.cos(math.radians(45))*self.dailRadi
        pointEndY = pos[1]+math.sin(math.radians(45))*self.dailRadi
        self.pointer1 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                                    vertices = [(pos[0],pos[1],0.0),
                                                (pointEndX, pointEndY,0.0)],
                                    line_width = self.lineWidth/2.0,
                                    material=(0/255,0/255,0/255))
        self.pointer1.set_shader(pi3d.Shader("mat_flat"))

        # Define Dail pointer 2
        pointEndX = pos[0]+math.cos(math.radians(30))*(self.dailRadi*0.8)
        pointEndY = pos[1]+math.sin(math.radians(30))*(self.dailRadi*0.8)
        self.pointer2 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                                    vertices = [(pos[0],pos[1],0.0),
                                                (pointEndX, pointEndY,0.0)],
                                    line_width = self.lineWidth/2.0,
                                    material=(0/255,0/255,0/255))
        self.pointer2.set_shader(pi3d.Shader("mat_flat"))

        # Define markings and numbers

        # Define center digital readout

    def createCircle(self):
        posX = self.pos[0]
        posY = self.pos[1]
        nVerts = 24
        angStep = 360.0/self.nVerts
        iAng = 0.0
        circleVerts = np.zeros((self.nVerts+1,3))
        for n in range(0,self.nVerts+1,1):
            circleVerts[n,0] = posX+math.cos(math.radians(iAng))*self.dailRadi
            circleVerts[n,1] = posY+math.sin(math.radians(iAng))*self.dailRadi
            iAng = iAng+angStep        
        return circleVerts

    def updateAndDraw(self):
        self.dail.set_line_width(self.lineWidth)
        self.dail.draw()
        self.pointer1.set_line_width(self.lineWidth)
        self.pointer1.draw()
        self.pointer2.set_line_width(self.lineWidth*2)
        self.pointer2.draw()
