import pi3d
import numpy as np
import math

### ----------------------------------------------------------------------------
# --- Analoge dail
class Dail(object):
    def __init__(self, pos, scaleStart, scaleEnd, scaleStep,
                 dailRadi=50,lineWidth=2.0, nVerts = 24):
        self.pos = pos
        self.scaleStart = scaleStart
        self.scaleEnd = scaleEnd
        self.scaleStep = scaleStep
        self.dailRadi = dailRadi
        self.lineWidth = lineWidth
        self.nVerts = nVerts
        
        # Define Dail circle
        circle = self.createCircle()
        self.dail = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                                    vertices = circle,
                                    line_width = self.lineWidth/2.0,
                                    material=(0/255,0/255,0/255))
        self.dail.set_shader(pi3d.Shader("mat_flat"))

        # Define Dail pointer 1
        inner_point_x = 0.0 + math.cos(math.radians(0.0))*self.dailRadi*0.4
        inner_point_y = 0.0 + math.sin(math.radians(0.0))*self.dailRadi*0.4
        inner_point_z = 0.0
        inner_point = (inner_point_x, inner_point_y, inner_point_z)

        
        outer_point_x = 0.0 + math.cos(math.radians(0.0))*self.dailRadi
        outer_point_y = 0.0 + math.sin(math.radians(0.0))*self.dailRadi
        outer_point_z = 0.0
        outer_point = (outer_point_x, outer_point_y, outer_point_z)
        
        self.pointer1 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                                    vertices = [inner_point, outer_point],
                                    line_width = self.lineWidth/2.0,
                                    material=(0/255,0/255,0/255))
        self.pointer1.set_shader(pi3d.Shader("mat_flat"))

##        self.cover_disc = pi3d.Disk(radius=0.2, sides=48,
##                                    x=0.9, y=1.9, z=0.1, rx=45)
##        self.cover_disc.set_shader(pi3d.Shader("mat_flat"))
##        self.cover_disc.set_material((200/255,200/255,200/255))

##        # Define Dail pointer 2
##        pointEndX = pos[0]+math.cos(math.radians(30))*(self.dailRadi*0.8)
##        pointEndY = pos[1]+math.sin(math.radians(30))*(self.dailRadi*0.8)
##        self.pointer2 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
##                                    vertices = [(pos[0],pos[1],0.0),
##                                                (pointEndX, pointEndY,0.0)],
##                                    line_width = self.lineWidth/2.0,
##                                    material=(0/255,0/255,0/255))
##        self.pointer2.set_shader(pi3d.Shader("mat_flat"))

        # Define markings and numbers

        # Define center digital readout

    def createCircle(self):
        posX = self.pos[0]
        posY = self.pos[1]
        nVerts = 24
        angStep = 360.0/self.nVerts
        iAng = 0.0
        circleVerts = np.zeros((self.nVerts+1,3))
        for n in range(0,self.nVerts+1,1):
            circleVerts[n,0] = posX+math.cos(math.radians(iAng))*self.dailRadi
            circleVerts[n,1] = posY+math.sin(math.radians(iAng))*self.dailRadi
            iAng = iAng+angStep        
        return circleVerts

    def updateAndDraw(self, p1_ang):
        self.dail.set_line_width(self.lineWidth)
        self.dail.draw()

        self.pointer1.set_line_width(self.lineWidth*3)
        self.pointer1.rotateToZ(-p1_ang+90.0)
        self.pointer1.translateX(self.pos[0])
        self.pointer1.translateY(self.pos[1])
        self.pointer1.draw()
        self.pointer1.translateX(-self.pos[0])
        self.pointer1.translateY(-self.pos[1])

##        self.cover_disc.draw()
        
##        self.pointer2.set_line_width(self.lineWidth*2)
##        self.pointer2.draw()
