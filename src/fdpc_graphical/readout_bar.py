import pi3d
import numpy as np
import math

### ----------------------------------------------------------------------------
# --- Readout Bar
class readoutBar(object):
    def __init__(self, startPoint, endPoint,
                 scaleStartValue, scaleEndValue,
                 lineWidth=4, scaleLineWidth=2, scaleMarkerFlip=False,
                 scaleZeroValue=0, limit_within_scale=False):

        # Bar information
        self.startPointX = startPoint[0]
        self.startPointY = startPoint[1]
        self.endPointX = endPoint[0]
        self.endPointY = endPoint[1]
        self.midPointX = (endPoint[0]+startPoint[0])/2.0
        self.midPointY = (endPoint[1]+startPoint[1])/2.0
        self.lineWidth = lineWidth
        directionVec = np.subtract(endPoint,startPoint)
        self.rotAngle = math.atan2(directionVec[1],directionVec[0])
        self.length = math.sqrt(math.pow(directionVec[0],2)+
                                math.pow(directionVec[1],2))
        self.scaleZeroValue = scaleZeroValue
        self.limit_within_scale = limit_within_scale

        # Reference scale information
        self.scaleStartValue = scaleStartValue
        self.scaleEndValue = scaleEndValue
        self.scaleLineWidth = scaleLineWidth
        self.scaleMarkerFlip = 1.0
        if scaleMarkerFlip:
            self.scaleMarkerFlip = -1.0
        self.scaleZeroValue = scaleZeroValue
        self.scaleStepLength = self.length/(scaleEndValue-scaleStartValue)

        # Calculate scale zero value x,y-coordinates
##        dx = self.scaleStepLength*math.cos(self.rotAngle)*abs(scaleStartValue)
##        dy = self.scaleStepLength*math.sin(self.rotAngle)*abs(scaleStartValue)
        dx = self.scaleStepLength*math.cos(self.rotAngle)*(scaleZeroValue-scaleStartValue)
        dy = self.scaleStepLength*math.sin(self.rotAngle)*(scaleZeroValue-scaleStartValue)
        
        self.scaleZeroX = self.startPointX+dx
        self.scaleZeroY = self.startPointY+dy
        

        # Initialize reference scale
        scaleVerts = self.createScaleVerts()
        self.refScale = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                                    vertices = scaleVerts,
                                    line_width = self.lineWidth/2.0,
                                    material=(0/255,0/255,0/255))
        self.refScale.set_shader(pi3d.Shader("mat_flat"))

        # Initialize readout bar
        initBarVerts = [(0-(self.length/2.0), 0.0, 0.0),
                        (0+(self.length/2.0), 0.0, 0.0)]
        self.bar = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                             vertices = initBarVerts,
                             line_width = self.lineWidth,
                             material=(0/255,0/255,0/255))
        self.bar.set_shader(pi3d.Shader("mat_flat"))

        # Not valid markings
        self.notValidMarkScale = 30#*notValidMarkScaleFact
        self.notValidMarkLW = 4#*self.notValidMarkScale
        self.notValidMark = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                                        vertices=[(self.midPointX-self.notValidMarkScale/2,    self.midPointY+self.notValidMarkScale/2,    0),
                                                 (self.midPointX+self.notValidMarkScale/2,    self.midPointY+self.notValidMarkScale/2,    0),
                                                 (self.midPointX+self.notValidMarkScale/2,    self.midPointY-self.notValidMarkScale/2,    0),
                                                 (self.midPointX-self.notValidMarkScale/2,    self.midPointY-self.notValidMarkScale/2,    0),
                                                 (self.midPointX-self.notValidMarkScale/2,    self.midPointY+self.notValidMarkScale/2,    0),
                                                 (self.midPointX+self.notValidMarkScale/2,    self.midPointY-self.notValidMarkScale/2,    0),
                                                 (self.midPointX+self.notValidMarkScale/2,    self.midPointY+self.notValidMarkScale/2,    0),
                                                 (self.midPointX-self.notValidMarkScale/2,    self.midPointY-self.notValidMarkScale/2,    0)],
                                    line_width=self.notValidMarkLW,
                                    material=(255/255,0/255,0/255))
        self.notValidMark.set_shader(pi3d.Shader("mat_flat"))



    # Draw function
    def updateAndDraw(self,valid,val):
        if valid == 1.0:
            # Draw reference scale
            self.refScale.set_line_width(self.scaleLineWidth)
            self.refScale.draw()

            # Set line width
            self.bar.set_line_width(self.lineWidth)

            # Calculate scale factor.
            # Limit bar to available scale
            if self.limit_within_scale:
                if val > self.scaleEndValue: val = self.scaleEndValue
                if val < self.scaleStartValue: val = self.scaleStartValue
            scaleFact = (val-self.scaleZeroValue)/(self.scaleEndValue-self.scaleStartValue)

            # Scale the bar
            self.bar.scale(scaleFact,1,0)

            # Rotate bar
            self.bar.rotateToZ(math.degrees(self.rotAngle))

            # Move to match reference scale
            currLength = scaleFact*(self.length)
            dx = (currLength*math.cos(self.rotAngle))/2.0
            dy = (currLength*math.sin(self.rotAngle))/2.0
            self.bar.positionX(self.scaleZeroX+dx)
            self.bar.positionY(self.scaleZeroY+dy)

            # Draw
            self.bar.draw()
        else:
            self.notValidMark.set_line_width(self.notValidMarkLW)
            self.notValidMark.draw()
        

    # Helper function to create the reference scale verticies
    def createScaleVerts(self):

        # Calculate number of verticies
        nVerts = (self.scaleEndValue-self.scaleStartValue)*3.0+3
        scaleVerts = np.zeros((int(nVerts),3))

        # Width of scale markings
        markerWidth = self.lineWidth*2*self.scaleMarkerFlip
            
        # Create standard array used for drawing verticies
        standardVert = np.zeros((3,3))

        # Create "standard L" to use for one step of scale
        markerDX = markerWidth*math.cos(self.rotAngle+math.pi/2)
        markerDY = markerWidth*math.sin(self.rotAngle+math.pi/2)
        offsetDX = -self.lineWidth*math.cos(self.rotAngle+math.pi/2)*self.scaleMarkerFlip
        offsetDY = -self.lineWidth*math.sin(self.rotAngle+math.pi/2)*self.scaleMarkerFlip
        standardVert[0] = (self.startPointX+offsetDX,    self.startPointY+offsetDY,   0)
        standardVert[1] = (self.startPointX+offsetDX+markerDX, self.startPointY+offsetDY+markerDY,0)
        standardVert[2] = (self.startPointX+offsetDX,    self.startPointY+offsetDY,   0)

        # Put together "L's" into scale
        dx = self.scaleStepLength*math.cos(self.rotAngle)
        dy = self.scaleStepLength*math.sin(self.rotAngle)
        
        for ii in range(0,int(nVerts)-1,3):
            scaleVerts[ii] = standardVert[0]
            scaleVerts[ii+1] = standardVert[1]
            scaleVerts[ii+2] = standardVert[2]
            standardVert[:,[0]] = standardVert[:,[0]]+dx
            standardVert[:,[1]] = standardVert[:,[1]]+dy
        return scaleVerts   
        
