import pi3d
import numpy as np
import math

# NavBall
class NavBall(object):
    """NavBall class. ADI (Attitude and Direction Indicator)"""
    def __init__(self,
                 textures,
                 shader,
                 radius,
                 pos=[0.0, 0.0, 0.0],
                 rot=[0.0, 0.0, 0.0],
                 track_shader=None,
                 light=None,
                 crossHairScaleFactor=1,
                 notValidMarkScaleFact=1,
                 showVelocityVector=0):

        self.radius = radius
        
        # Define ADI (Attitude and Directional Indicator), the NavBall
        self.adi = pi3d.Sphere(radius=radius, slices=64, sides=64,
                             x=pos[0], y=pos[1], z=pos[2],
                             rx=rot[0],ry=rot[1],rz=rot[2])
        self.adi.set_draw_details(shader,[textures])
        if light is not None:
            self.adi.set_light(light)
        self.adi.shell = None
        self.adi.track_shader = track_shader

        # Save position
        self.posX = pos[0]
        self.posY = pos[1]
        self.posZ = pos[2]

        


        # Define Crosshair
        verts = [[0.0, 0.0, 1.1], [0.0, 1.0, 1.1]]
        self.crossHair = pi3d.Lines(vertices = verts)
        self.crossHair.set_shader(pi3d.Shader("mat_flat"))
        self.crossHairScale = radius*crossHairScaleFactor
        self.crossHairLineWidth = 4*self.crossHairScale
        crossHairVerts = self.createCrossHairVerts(pos,self.crossHairScale,radius)
        self.crossHair = pi3d.Lines(vertices=crossHairVerts,
                                    light=light,
                                    line_width=self.crossHairLineWidth,
                                    material=(255/255,255/255,0/255))
        self.crossHair.set_shader(pi3d.Shader("mat_light"))

        self.crossHairFrame = pi3d.Lines(vertices=crossHairVerts,
                                    light=light,
                                    line_width=self.crossHairLineWidth,
                                    material=(0/255,0/255,0/255))


        # Define Velocity vector
        vel_vect_verts = self.createVelocityVector([0.0, 0.0, 0.0],radius)
        self.vel_vect = pi3d.Lines(vertices=vel_vect_verts,
                                    light=light,
                                    line_width=self.crossHairLineWidth,
                                    material=(0/255,0/255,0/255))
        self.vel_vect.set_shader(pi3d.Shader("mat_light"))


        # Define roll indicator
        rollVerts = self.createRollMarkVerts([0.0, 0.0, 0.0],radius)
        self.rollMarks = pi3d.Lines(vertices=rollVerts,
                                    light=light,
                                    line_width=self.crossHairLineWidth,
                                    material=(0/255,0/255,0/255))
        self.rollMarks.set_shader(pi3d.Shader("mat_light"))


        # Roll triangle
        roll_triang_shift = self.posY-radius
        self.roll_triang_frame = pi3d.Triangle(
            corners=((0.0,1.85+roll_triang_shift),
                     (0.1,1.7+roll_triang_shift),
                     (-0.1,1.7+roll_triang_shift)),
            z=-radius)
        self.roll_triang_frame.set_shader(pi3d.Shader("mat_flat"))
        self.roll_triang_frame.set_material((0/255,0/255,0/255))

        self.roll_triang_fill = pi3d.Triangle(
            corners=((0.0,1.8+roll_triang_shift),
                     (0.051,1.72+roll_triang_shift),
                     (-0.051,1.72+roll_triang_shift)),
            z=-1.05)
        self.roll_triang_fill.set_shader(pi3d.Shader("mat_flat"))
        self.roll_triang_fill.set_material((1.0,1.0,1.0))


        # Not valid markings
        self.notValidMark = pi3d.Lines(vertices = verts)
        self.notValidMark.set_shader(pi3d.Shader("mat_flat"))
        self.notValidMarkScale = radius*notValidMarkScaleFact
        self.notValidMarkLW = 4*self.notValidMarkScale
        notValidVerts = self.createNotValidMarks(pos,self.notValidMarkScale,radius)
        self.notValidMark = pi3d.Lines(vertices=notValidVerts,
                                    light=light,
                                    line_width=self.crossHairLineWidth,
                                    material=(255/255,0/255,0/255))
    # END __init___


    # Draw function.
    # This function updates attitude and draws all objects.
    #def rotateAndDraw(self, valid, yaw, pitch, roll, nx=0, ny=0, nz=0, aoa=0, aos=0):
    def rotateAndDraw(self, valid, yaw, pitch, roll, nx=0, ny=0, nz=0, aoa=0, aos=0):    
        # Draw ADI
        if valid == 1.0:
            self.adi.rotateToY(yaw)
            self.adi.rotateToX(-pitch)
            self.adi.rotateToZ(roll)
            self.adi.draw()

            # Draw crosshair
            self.crossHair.set_line_width(self.crossHairLineWidth)
            self.crossHair.draw()
            self.crossHairFrame.set_line_width(self.crossHairLineWidth*2)
            self.crossHairFrame.draw()

            # Draw velocity vector
            self.vel_vect.set_line_width(4.0)
            self.vel_vect.rotateToZ(90.0)
            self.vel_vect.translateY(self.posY)
            self.vel_vect.translateX(math.sin(math.radians(aos))*self.radius)
            self.vel_vect.translateY(-math.sin(math.radians(aoa))*self.radius)
            self.vel_vect.draw()
            self.vel_vect.translateY(math.sin(math.radians(aoa))*self.radius)
            self.vel_vect.translateX(-math.sin(math.radians(aos))*self.radius)
            self.vel_vect.translateY(-self.posY)

            # Draw roll marks
            self.rollMarks.set_line_width(self.crossHairLineWidth*0.5)
            self.rollMarks.rotateToZ(roll+90.0)
            self.rollMarks.translateY(self.posY)
            self.rollMarks.draw()
            self.rollMarks.translateY(-self.posY)

            # Draw roll triangel
            self.roll_triang_frame.draw()
            self.roll_triang_fill.draw()



        # Draw not valid
        else:
            self.notValidMark.set_line_width(self.notValidMarkLW)
            self.notValidMark.draw()
    # END rotateAndDraw


    # Create cross hair verticies
    def createCrossHairVerts(self,pos,scale,radius):
        verts = [(pos[0]-scale/2,    pos[1],           pos[2]-radius-0.1),
                 (pos[0]-scale/4,    pos[1],           pos[2]-radius-0.1),
                 (pos[0],            pos[1]-scale/4,   pos[2]-radius-0.1),
                 (pos[0],            pos[1],           pos[2]-radius-0.1),
                 (pos[0],            pos[1]-scale/4,   pos[2]-radius-0.1),
                 (pos[0]+scale/4,    pos[1],           pos[2]-radius-0.1),
                 (pos[0]+scale/2,    pos[1],           pos[2]-radius-0.1)]
        return verts
    # END createCrossHairVerts


    # Create not valid marking verticies
    def createNotValidMarks(self,pos,scale,radius):
        verticies=[(pos[0]-scale/2,    pos[1]+scale/2,    pos[2]-radius-0.1),
                (pos[0]+scale/2,    pos[1]+scale/2,    pos[2]-radius-0.1),
                (pos[0]+scale/2,    pos[1]-scale/2,    pos[2]-radius-0.1),
                (pos[0]-scale/2,    pos[1]-scale/2,    pos[2]-radius-0.1),
                (pos[0]-scale/2,    pos[1]+scale/2,    pos[2]-radius-0.1),
                (pos[0]+scale/2,    pos[1]-scale/2,    pos[2]-radius-0.1),
                (pos[0]+scale/2,    pos[1]+scale/2,    pos[2]-radius-0.1),
                (pos[0]-scale/2,    pos[1]-scale/2,    pos[2]-radius-0.1)]
        return verticies
    # END createNotValidMarks


    # Create roll markings verticies
    def createRollMarkVerts(self,pos,radius):

        # Center position of circle
        posX = pos[0]
        posY = pos[1]
        posZ = pos[2]-(radius*0.1)

        # Angles to mark with lines
        markAngles = np.zeros((6,1))
        markAngles[:,0] = [10, 20, 30, 45, 60, 90]

        # Calculate nr of vertices needed
        angStep = 5
        nVerts = 360/angStep + markAngles.size*3 + 10

        # Starting angle, and radius of circle
        iAng = -180.0
        markSize = 1.08
        startAng = iAng
        endAng = -iAng

        # Create markings
        rollMarks = np.zeros((int(nVerts),3))
        n=0
        while iAng<=180:

            # Populate rollMarks matrix
            rollMarks[n,0] = posX+math.cos(math.radians(iAng))*radius
            rollMarks[n,1] = posY+math.sin(math.radians(iAng))*radius
            rollMarks[n,2] = posZ

            # When reaching an angle to mark, create a line
            if abs(iAng) in markAngles:

                # Emphisize 90, 60, 30 by longer lines
                markSize = 1.05
                if abs(iAng) in [90.0, 60.0, 30.0]:
                    markSize = 1.09
                
                rollMarks[n+1,0] = posX+math.cos(math.radians(iAng))*(radius*markSize)
                rollMarks[n+1,1] = posY+math.sin(math.radians(iAng))*(radius*markSize)
                rollMarks[n+1,2] = posZ

                # Jump back to point on circle
                rollMarks[n+2,0] = rollMarks[n,0]
                rollMarks[n+2,1] = rollMarks[n,1]
                rollMarks[n+2,2] = rollMarks[n,2]

                # Jump two steps
                n = n+2

            if iAng == 0:
                rollMarks[n+1,:] = [rollMarks[n,0]+radius/15,
                               rollMarks[n,1]-radius/15,
                               posZ-0.2]
                rollMarks[n+2,:] = [rollMarks[n,0]+radius/15,
                               rollMarks[n,1]+radius/15,
                               posZ-0.2]
                rollMarks[n+3,0] = rollMarks[n,0]
                rollMarks[n+3,1] = rollMarks[n,1]
                rollMarks[n+3,2] = rollMarks[n,2]

                # Jump three steps
                n = n+3
                

            # Step to next angle
            iAng = iAng+angStep
            n=n+1

        # Return vertices
        return rollMarks
    # END createRollMarks



    # Create roll markings verticies
    def createVelocityVector(self,pos,radius):

        # Size of VV circle
        vv_circ_radi_fact = 1/20

        # Center position of circle
        posX = pos[0]
        posY = pos[1]
        posZ = pos[2]-radius-0.01

        # Angles to mark with lines
        markAngles = np.zeros((2,1))
        markAngles[:,0] = [0, 90]

        # Calculate nr of vertices needed
        angStep = 5
        nVerts = 360/angStep + markAngles.size*3 + 1

        # Starting angle, and radius of circle
        iAng = -180.0
        markSize = (radius*vv_circ_radi_fact)+0.1
        startAng = iAng
        endAng = -iAng

        # Create markings
        rollMarks = np.zeros((int(nVerts),3))
        n=0
        while iAng<=180:

            # Populate rollMarks matrix
            rollMarks[n,0] = posX+math.cos(math.radians(iAng))*(radius*vv_circ_radi_fact)
            rollMarks[n,1] = posY+math.sin(math.radians(iAng))*(radius*vv_circ_radi_fact)
            rollMarks[n,2] = posZ

            # When reaching an angle to mark, create a line
            if abs(iAng) in markAngles:
                rollMarks[n+1,0] = posX+math.cos(math.radians(iAng))*(radius*markSize)
                rollMarks[n+1,1] = posY+math.sin(math.radians(iAng))*(radius*markSize)
                rollMarks[n+1,2] = posZ

                # Jump back to point on circle
                rollMarks[n+2,0] = rollMarks[n,0]
                rollMarks[n+2,1] = rollMarks[n,1]
                rollMarks[n+2,2] = rollMarks[n,2]

                # Jump two steps
                n = n+2
               

            # Step to next angle
            iAng = iAng+angStep
            n=n+1

        # Return vertices
        return rollMarks
    # END createRollMarks
    

