import pi3d
import numpy as np
import math

### ----------------------------------------------------------------------------
# --- Digital Readout
class digitalReadout(object):
    def __init__(self, pos, initStringFormat, font, data,
                 fontSize=20, color=(0,0,0,255)):
        self.pos = pos
        self.initStringFormat = initStringFormat
        self.font = font
        self.fontSize = fontSize
        self.color = color

        # Define readout format
        readoutFont = pi3d.Font(self.font,
                                font_size = self.fontSize,
                                color=self.color)
        readoutFont.blend = False

        # Draw string
        self.readout = pi3d.String(font = readoutFont,
                                   camera=pi3d.Camera(is_3d=False),
                                   is_3d=False,
                                   string=self.initStringFormat.format(data[0]),
                                   x=self.pos[0],
                                   y=self.pos[1],
                                   z=self.pos[2])
        self.readout.set_shader(pi3d.Shader("uv_flat"))
        self.readout.draw()
        

    def updateAndDraw(self,data):
        self.readout.quick_change(self.initStringFormat.format(data))
        self.readout.draw()
