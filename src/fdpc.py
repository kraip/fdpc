#!/usr/bin/python
"""
Main file for the Flight Data and Planning Computer
Created by: Christian Krantz
"""

### -------------------------------------------------------------------|
# --- Import
# Libraries
import math
import pi3d
import serial
import serial.tools.list_ports
import threading
import time
import numpy as np
import RPi.GPIO as GPIO

# FDPC packages
from fdpc_graphical.navball import NavBall
from fdpc_graphical.altitude_dail import Dail
from fdpc_graphical.digital_readout import digitalReadout
from fdpc_graphical.readout_bar import readoutBar
from fdpc_sensors.ahrs import AHRS
from fdpc_sensors.gps import GPS
##from fdpc_filter.moving_average import moving_average


##import rpi_backlight as bl
##bl.set_brightness(255)


### -------------------------------------------------------------------|
# --- Constants
DEG2RAD = math.pi/180.0


### -------------------------------------------------------------------|
# --- Colors and fonts
COLOR_DAY_BACKGROUND = np.array([255/255,255/255,204/255])
COLOR_DARK_LINES = np.array([0.0, 0.0, 0.0])/255
STD_FONT = '../fonts/Courier Prime Sans.ttf'
##STD_FONT = '../fonts/MS33558.ttf'
font = pi3d.Font(STD_FONT, color=(0,0,0,255), font_size=20)



### -------------------------------------------------------------------|
# --- Display-, camera- and shader settings
DISP_POS_X = 0          # Position X
DISP_POS_Y = 0          # Position Y
DISP_FPS = 60           # Framerate
DISP_ANTIALIASING = 4   # Anti-alaising ON, (OFF = 0)
DISP_WIDTH = 480        # Width
DISP_HEIGHT = 800       # Height
DISPLAY = pi3d.Display.create(x = DISP_POS_X, y = DISP_POS_Y,
                              frames_per_second = DISP_FPS,
                              samples = DISP_ANTIALIASING)
DISPLAY.set_background(COLOR_DAY_BACKGROUND[0],
                       COLOR_DAY_BACKGROUND[1],
                       COLOR_DAY_BACKGROUND[2],1)
DISPLAY.resize(x=DISP_POS_X,y=DISP_POS_Y ,w=DISP_WIDTH,h=DISP_HEIGHT)

DISP_SHADER_FLAT = pi3d.Shader("uv_flat")   # 3D Shader flat
DISP_CAM_POS = (0.0,1.0,-6.0)             # Camera- & ADI position
DISP_CAM_DIRECTION = (0.0,0.0,0.0)          # Where camera is pointing
DISP_CAMERA_3D = pi3d.Camera(at = DISP_CAM_DIRECTION,   # Create camera
                             eye = DISP_CAM_POS)


### -------------------------------------------------------------------|
# --- Create screen ref lines
# Vertical line center
midLineX = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(0.0,DISP_HEIGHT/2,0.0),
                                        (0.0,-DISP_HEIGHT/2,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineX.set_shader(pi3d.Shader("mat_flat"))

# Horizontal line center
midLineYC = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(-DISP_WIDTH/2,0.0,0.0),
                                        (DISP_WIDTH/2,0.0,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineYC.set_shader(pi3d.Shader("mat_flat"))

# Horizontal line 1/4rd
midLineY14 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(-DISP_WIDTH/2,DISP_HEIGHT/4,0.0),
                                        (DISP_WIDTH/2,DISP_HEIGHT/4,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineY14.set_shader(pi3d.Shader("mat_flat"))

# Horizontal line 3/4th
midLineY34 = pi3d.Lines(camera=pi3d.Camera(is_3d=False),
                            vertices = [(-DISP_WIDTH/2,-DISP_HEIGHT/4,0.0),
                                        (DISP_WIDTH/2,-DISP_HEIGHT/4,0.0)],
                            line_width = 2.0,
                            material=COLOR_DARK_LINES)
midLineY34.set_shader(pi3d.Shader("mat_flat"))



### -------------------------------------------------------------------|
# --- Create ADI/NavBall
ADI_SELFLIGHT = pi3d.Light(lightamb=(1.1, 1.1, 1.1))
ADI_TEXTURE = pi3d.Texture("../textures/navball_new.png")
ADI_RADI = 1.0
ADI_POS = (DISP_CAM_POS[0],DISP_CAM_POS[1],0)
ADI_CROSSHAIR_SCALE = 1.2
adi = NavBall(ADI_TEXTURE, DISP_SHADER_FLAT, ADI_RADI,
              light = ADI_SELFLIGHT,
              pos = ADI_POS,
              crossHairScaleFactor = ADI_CROSSHAIR_SCALE)

### -------------------------------------------------------------------|
# --- Create analoge dails
altDail = Dail([180.0,340,0],0.0,10.0,1.0,dailRadi=40,nVerts=48)
speedDail = Dail([-180.0,340,0],0.0,10.0,1.0,dailRadi=40,nVerts=48)


### -------------------------------------------------------------------|
# --- Create digital readouts
yawReadout = digitalReadout([0.0,-40.0,0.0],"HDG: {:03.0f}",STD_FONT,[360.0])
pitchReadout = digitalReadout([0.0,-60.0,0.0],"PCH: {:3.0f}",STD_FONT,[90.0])
rollReadout = digitalReadout([0.0,360.0,0.0],"ROL: {:4.0f}",STD_FONT,[-180.0])
nzReadout = digitalReadout([-180.0,0.0,0.0],"G: {:4.1f}",STD_FONT,[90.0])
##vSpeedReadout = digitalReadout([180.0,0.0,0.0],"m/s: {:4.1f}",STD_FONT,[90.0])


# GPS
clockPosX = 0.0
clockPosY = 380.0
clockFontSize = 14
utchReadout = digitalReadout([clockPosX-19,clockPosY,0.0],
                             "{:02.0f}",
                             STD_FONT,
                             [00.0],
                             fontSize=clockFontSize)
utcmReadout = digitalReadout([clockPosX,clockPosY,0.0],
                             ":{:02.0f}",
                             STD_FONT,
                             [00.0],
                             fontSize=clockFontSize)
utcsReadout = digitalReadout([clockPosX+21,clockPosY,0.0],
                             ":{:02.0f}",
                             STD_FONT,
                             [00],
                             fontSize=clockFontSize)


gpsLatPosX = -140.0
gpsLatPosY = -100.0
deg_sign = u'\xb0'
gps_latDMS_ro = digitalReadout([gpsLatPosX,gpsLatPosY,0.0],
                               "{}",
                               STD_FONT,
                               ['XXXXXXXXXXXXXXXX'])
gpsLonPosX = -140.0
gpsLonPosY = -120.0
gps_lonDMS_ro = digitalReadout([gpsLonPosX,gpsLonPosY,0.0],
                               "{}",
                               STD_FONT,
                               ['XXXXXXXXXXXXXXXX'])

gps_hEll_ro = digitalReadout([-gpsLatPosX,gpsLatPosY,0.0],
                               "H_ell: {:5.0f}m",
                               STD_FONT,
                               [88888])
gps_hGeo_ro = digitalReadout([-gpsLonPosX,gpsLonPosY,0.0],
                               "H_Geo: {:5.0f}m",
                               STD_FONT,
                               [88888])

gps_GS_ro = digitalReadout([-gpsLonPosX+18,gpsLonPosY-20,0.0],
                               "GS: {:3.0f}kmh",
                               STD_FONT,
                               [888])
gps_TT_ro = digitalReadout([-gpsLonPosX+8,gpsLonPosY-40,0.0],
                               "TT: {:03.0f}\xb0",
                               STD_FONT,
                               [888])



accy_text_posX = -190
accy_text_posY = gpsLonPosY

gps_nSat_ro = digitalReadout([accy_text_posX-8,accy_text_posY-56,0.0],
                               "nSV: {:2.0f}",
                               STD_FONT,
                               [999.9],
                               fontSize=14)
gps_fix_ro = digitalReadout([accy_text_posX-12,accy_text_posY-68,0.0],
                               "FIX: {}",
                               STD_FONT,
                               ['1'],
                               fontSize=14)
gps_valid_ro = digitalReadout([accy_text_posX,accy_text_posY-80,0.0],
                               "GPS valid: {:1.0f}",
                               STD_FONT,
                               [0.0],
                               fontSize=14)


### -------------------------------------------------------------------|
# --- Create readout bar
nzPosLim = 5.0
nzNegLim = 0.0
nzBar = readoutBar([-190.0,20.0,0.0],[-190.0,300.0,0.0],
                    nzNegLim,nzPosLim, lineWidth=6, scaleMarkerFlip=True)

sideSlipPosLim = 2.0
sideSlipNegLim = -2.0
sideSlipBar = readoutBar([-100.0,-20.0,0.0],[100.0,-20.0,0.0],
                    sideSlipNegLim,sideSlipPosLim, lineWidth=6, scaleMarkerFlip=False)


### -------------------------------------------------------------------|
# --- Serial test
ports = list(serial.tools.list_ports.comports())
print('Available com-port units:')
for p in ports:
    print(p)


### -------------------------------------------------------------------|
# --- Set up serial communication and initialize AHRS over UART
AHRS_UART = serial.Serial(port='/dev/ttyUSB0', baudrate = 57600, timeout=0.02)
ahrs = AHRS(AHRS_UART)
ahrs.start()


### -------------------------------------------------------------------|
# --- GPS setup
gpsSerial = serial.Serial(port='/dev/serial0', baudrate = 9600, timeout = 0.02)
gps = GPS(gpsSerial)
gps.start()


### -------------------------------------------------------------------|
# --- Main function
def main():

    test_altitude_ang = 0.0

        
    # Start loop
    while DISPLAY.loop_running():

##        if GPIO.event_detected(17):
##            print('Tick')
##
##        if GPIO.event_detected(27):
##            print('GPS OK')




        # Draw screen ref lines
##        midLineX.set_line_width(2.0)
##        midLineX.draw()
##        midLineYC.set_line_width(2.0)
##        midLineYC.draw()
##        midLineY14.set_line_width(1.0)
##        midLineY14.draw()
##        midLineY34.set_line_width(1.0)
##        midLineY34.draw()
                        
        # Draw and rotate NavBall
        ahrsData = ahrs.data_vec
        adi.rotateAndDraw(ahrsData[0],
                          ahrsData[1]/0.01745,
                          ahrsData[2]/0.01745,
                          ahrsData[3]/0.01745)

        # Draw analoge dails
##        adiDail.updateAndDraw()
        altDail.updateAndDraw(test_altitude_ang)
        speedDail.updateAndDraw(2*test_altitude_ang+45.0)
        test_altitude_ang = test_altitude_ang + 1

        # Draw digital readout
        yawReadout.updateAndDraw((ahrsData[1]/0.01745+360.0)%360)
        pitchReadout.updateAndDraw(ahrsData[2]/0.01745)
        rollReadout.updateAndDraw(ahrsData[3]/0.01745)
        nzReadout.updateAndDraw(ahrsData[6])

        # Draw and update nz Bar
        nzBar.updateAndDraw(ahrsData[0],ahrsData[6])
        sideSlipBar.updateAndDraw(ahrsData[0],(math.degrees(ahrsData[7])/180))

        # Draw GPS data
        utchReadout.updateAndDraw(gps.UTC_h)
        utcmReadout.updateAndDraw(gps.UTC_m)
        utcsReadout.updateAndDraw(gps.UTC_s)
        gps_latDMS_ro.updateAndDraw(gps.lat_dms)
        gps_lonDMS_ro.updateAndDraw(gps.lon_dms)
        gps_nSat_ro.updateAndDraw(gps.n_satellites)
        gps_fix_ro.updateAndDraw(gps.mode_2)
        gps_hEll_ro.updateAndDraw(gps.h_ell)
        gps_hGeo_ro.updateAndDraw(gps.h_geo)
        gps_GS_ro.updateAndDraw(gps.GS*3.6)
        gps_TT_ro.updateAndDraw(gps.TA)
        gps_valid_ro.updateAndDraw(gps.validity)

        
        
    # Close Serial port    
    AHRS_UART.close()
    gpsSerial.close()
    DISPLAY.destroy()
    GPIO.cleanup()



### -------------------------------------------------------------------|
# --- Exexcution
if __name__ == '__main__':main()






