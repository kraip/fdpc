##import serial
import numpy as np
##import math
##import time

class moving_average(object):
    """Moving average filter
    Default is a moving average of 10 samples.
    """

    def __init__(self,nSamples=10):
        """Attributes are:
        n_samples - number of samples to use for moving average
        old_data - vector for storing data history
        fild_data - the filtered data
        """
        
        self.nSamples = nSamples
        self.old_data = np.zeros((nSamples,1))
        self.filt_data = 0.0
        

    def update(self,in_data):
        """Update method. Used to update the filter, and calculate
        a new moving average.
        """
        
        self.old_data[:self.nSamples-1] = self.old_data[1:self.nSamples]
        self.old_data[self.nSamples-1] = in_data
        self.filt_data = np.mean(self.old_data)
        
    @property
    def filtered_data(self):
        """Moving average filtered data"""
        
        return self.filt_data


class exp_smooth(object):
    """Exponential smoothening filter"""

    def __init__(self, factor=0.5):
        """Attributes are:
        factor - smoothening factor, in range [0 1]. Where 1 results
        in no filtering
        s_t - Exponentially filtered value
        s_t_old - Value of filter from last time step
        """

        self.factor = factor
        self.st = 0
        self.st_old = 0


    def update(self, in_data):
        """Update method. Used to update filter and calculate
        an exponentially smoothed value.
        """

        self.st = self.factor*in_data + (1-self.factor)*self.st


    @property
    def filtered_data(self):
        """Exponentially filtered data"""

        return self.st

    

     
            
            






        
