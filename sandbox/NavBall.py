#!/usr/bin/python
##from __future__ import absolute_import, division, print_function, unicode_literals
##""" Model gravitational attraction between bodies. Uses a Planet class to
##tidy code
##"""
import numpy as np
import math
#from numpy import sqrt, sin, cos, radians, pi, add, subtract, array, tile
##import demo
import pi3d
import sys
sys.path.insert(1, '/home/pi/code/fdpc')
##################################################################
# Planet  class based on a pi3d.Sphere but with some added properties
class Planet(pi3d.Sphere):
  def __init__(self, textures, shader, radius, pos=[0.0, 0.0, 0.0],
              track_shader=None, light=None):
    super(Planet, self).__init__(radius=radius, slices=24, sides=24,
                                 x=pos[0], y=pos[1], z=pos[2])
    super(Planet, self).set_draw_details(shader, [textures[0]])
    if light is not None:
      self.set_light(light)
    self.shell = None
    self.track_shader = track_shader
    self.rotateToY(0)  #yaw
    self.rotateToX(0)   #pitch
    self.rotateToZ(0)   #roll

        
  def position_and_draw(self):
    self.draw()

# Setup display and initialise pi3d ------
DISPLAY = pi3d.Display.create(x=0, y=0, frames_per_second=20)
DISPLAY.set_background(0,0,0,1)    	# r,g,b,alpha
DISPLAY.resize(x=0,y=0,w=800,h=480)

# Camera ---------------------------------
CAMERA = pi3d.Camera()
CAM2D = pi3d.Camera(is_3d=False)

# Shaders --------------------------------
shader = pi3d.Shader("uv_light")
##flatsh = pi3d.Shader("uv_flat")
##tracksh = pi3d.Shader("mat_flat")

# Textures -------------------------------
sunimg = pi3d.Texture("textures/navball_std.png")

# Lights ---------------------------------
selflight = pi3d.Light(lightamb=(1.1, 1.1, 1.1))

# Planets --------------------------------
sun = Planet([sunimg], shader, 4.0, pos=[0.0, 0.0, 0.0], light=selflight)

# Fetch key presses ----------------------
##mykeys = pi3d.Keyboard()

# Mouse ----------------------------------
mymouse = pi3d.Mouse(restrict = False)
mymouse.start()

# Camera variables -----------------------
rot = 0
tilt = 0
rottilt = True
camRad = [-13.5, -13.5, -13.5]

# Display scene
fr = 0
while DISPLAY.loop_running():
  if rottilt:
    CAMERA.relocate(rot, tilt, [0.0, 0.0, 0.0], camRad)
    rottilt = False

  sun.position_and_draw()

  mx, my = mymouse.position()
  if rot != (mx * -0.1) or tilt != (my * 0.1):
    rot = mx * -0.1
    tilt = my * 0.1
    rottilt = True

##  k = mykeys.read()
##  if k >-1:
##    rottilt = True
##    if k==112:
##      pi3d.screenshot("orbit.jpg")
##    elif k==61:   #key += in
##      camRad = [r + 0.5 for r in camRad]
##    elif k==45:   #key _- out
##      camRad = [r - 0.5 for r in camRad]
##    elif k==27:
##      mykeys.close()
##      DISPLAY.destroy()
##      break

