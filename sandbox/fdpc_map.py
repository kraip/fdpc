##import folium
##m = folium.Map(location=[58.3944176,15.6223068],
##               tiles='Stamen Terrain',
##               prefer_canvas=True)
##print(folium.__version__)
##m.save('map.html')
##m



##import folium  
##from IPython.display import HTML, display
##LDN_COORDINATES = (51.5074, 0.1278) 
##myMap = folium.Map(location=LDN_COORDINATES, zoom_start=12)
##myMap._build_map()
##mapWidth, mapHeight = (400,500) # width and height of the displayed iFrame, in pixels
##srcdoc = myMap.HTML.replace('"', '&quot;')
##embed = HTML('<iframe srcdoc="{}" '
##             'style="width: {}px; height: {}px; display:block; width: 50%; margin: 0 auto; '
##             'border: none"></iframe>'.format(srcdoc, width, height))
##embed


import os
import folium

print(folium.__version__)

from branca.element import Figure
lon, lat = -122.1889, 46.1991
location = [lat, lon]
zoom_start = 13
tiles = 'OpenStreetMap'

width, height = 480, 350
fig = Figure(width=width, height=height)
m = folium.Map(
    location=location,
    tiles=tiles,
    width=width,
    height=height,
    zoom_start=zoom_start
)

fig.add_child(m)
##fig.save(os.path.join('results', 'WidthHeight_0.html'))
fig.render()
